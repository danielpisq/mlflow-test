#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sklearn.datasets import load_boston
from sklearn.linear_model import ElasticNet
from sklearn.model_selection import train_test_split

from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score

import mlflow
import mlflow.sklearn


# In[2]:


(X, y) = load_boston(True)

X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2)


# In[ ]:





# In[ ]:





# In[3]:


print(X_test.shape, y_test.shape)


# In[16]:


alpha = 0.6
l1_ratio = 0.6

regr = ElasticNet(alpha=alpha, l1_ratio=l1_ratio)


regr.fit(X_train ,y_train)

y_pred = regr.predict(X_test)


evs = explained_variance_score(y_test, y_pred)
r2 = r2_score(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)


# In[20]:


print("mse: {}\nevs: {}\nr2: {}".format(mse, evs, r2))


mlflow.log_param("alpha", alpha)
mlflow.log_param("l1_ratio", l1_ratio)
mlflow.log_metric("rmse", evs)
mlflow.log_metric("r2", r2)
mlflow.log_metric("mae", mse)

mlflow.sklearn.log_model(regr, "model")



